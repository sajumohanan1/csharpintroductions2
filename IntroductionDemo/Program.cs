﻿using System; //use classes from system name space

namespace IntroductionDemo//as a container for the class
{
    class Program//is a container for data and methods (functions)
    {
        static void Main(string[] args)//as the main method
        {
          Console.WriteLine("Hello World!"); //to print output in console window

            // int -> integer type of data (1,2,3)
            // string -> string type of data (tv, fridge, smartphone)
            // character -> string (A, B, Z)

            //Create a variable
            //To store some values temporarly for some kind of process

            //int itemQuantity; //varialbe has been declared
            //itemQuantity = 10; //the value assigned to variable
            //Console.WriteLine(itemQuantity); //value printed from variable (memory) to the console window

            //create variable as lnline
            int itemQuantity = 10;
            Console.WriteLine("Item Quantity : " + itemQuantity); //string concatenation
            Console.WriteLine($"Item Quantity : {itemQuantity}"); //string interpolation - $

            string itemName = "smart phone"; //double quotes represent a string
            Console.WriteLine(itemName);

            char code = 'A'; //single character
            Console.WriteLine(code);

            bool itemAvailability = true;
            Console.WriteLine(itemAvailability);

            //Type conversion
            string numberAsWord = "594"; //Convert this string to number
            int number = int.Parse(numberAsWord);
            Console.WriteLine("This is a number right now " + number);

            int mobNumber = 9898998; //Covert this number as a string
            string mobnumberAsWord = mobNumber.ToString();
            Console.WriteLine("This is a string right now " + mobnumberAsWord);

            int[] itemQty = new int[4]; //declare one dimenstional array
            itemQty[0] = 25;//Value 25 is moving to array index 0 (n-1)
            itemQty[1] = 15;
            itemQty[2] = 125;
            itemQty[3] = 2;
            Console.WriteLine(itemQty[1]);

           

            //C sharp is case sensitive - a, A are two different variable

            //Error handling --> exception handling
            try
            {
                string[] productName = new string[] { "Television", "Smart Phone" };
                Console.WriteLine(productName[0] + ", " + productName[2]);
            }
            catch(IndexOutOfRangeException ex)
            {
                Console.WriteLine("Incorret Index Reference");
            }
            catch (Exception ex)
            {
                Console.WriteLine("An error has occured");
            }
            finally
            {
                Console.WriteLine("This code will always run");
            }

            //IndexOutOfRangeException - 

            int[,] ages = new int[,] //two dimensional array
            {
                {10,12,13}, //first row index=0 - columns-0,1,2
                {88,2,1}//second row index=1- columns-0,1,2
            };
            Console.WriteLine("Value of row 1 and col 1 will be printed "+ages[0,0]);

            //Generics - type of collection
            //Generics is having variable size - more flexible

            /* Dictionary
             * List
             * SortedList
             * Queue
             * Stack
             */

            //Moduliarization --> breaking up logic into units of work. 
            //Methods --> functions

            //call the method here
            Addition(10.67,15.4,14); //call the method 
            Addition(2,15.55,2); //reuse 
            Addition(3,4,3); //reuse 
            Addition(1,2,4); //reuse 

            static void Addition(double number1, double number2, double number3) //create a method - to add two numbers
            {
                double result = number1 + number2 + number3; //Process
                Console.WriteLine($"Result : {result}");
            }

            // (1) Create a Method (2) Use the method / Invoke the Method many times

        }


    }
   
    
    

     

}
